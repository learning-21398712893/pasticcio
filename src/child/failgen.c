#include "../../lib/mainlib.h"
#include "../../lib/childlib.h"

#define PROB(n) (rand() % (n)) == 0

int masterChannel;
char buffer[BUFFERSIZE];

int PFC[3];

FILE *logFile;

// ----------------------------------------------------------------------------------------- Main

int main() {
    srand(time(0));
    childMain(&masterChannel, buffer, "failgen");
}

int setup() {
    logFile = fopen("../logs/failures.log", "w");
    return logFile == NULL;
}

int preparation() {
    // do nothing
    return 0;
}

int connection() {
    read(masterChannel, buffer, BUFFERSIZE);
    char *pfc = strtok(buffer, " ");
    int i = 0;
    while (pfc != NULL) {
        PFC[i++] = parseInt(pfc);
        pfc = strtok(NULL, " ");
    }
    return PFC[0] <= 0 && PFC[1] <= 0 && PFC[2] <= 0;
}

int routine() {
    int pfcToKill = rand() % 3 + 1;
    int pidToKill = PFC[pfcToKill - 1];
    fprintf(logFile, "DISTURBO PFC%d |", pfcToKill);
    int errors = 0;
    if (PROB(100)) { // SIGSTOP
        kill(pidToKill, SIGSTOP);
        fprintf(logFile, "SIGSTOP ");
        errors++;
    }
    if (PROB(10000)) { // SIGINT
        kill(pidToKill, SIGINT);
        fprintf(logFile, "SIGINT ");
        errors++;
    }
    if (PROB(10)) { // SIGCONT
        kill(pidToKill, SIGCONT);
        fprintf(logFile, "SIGCONT ");
        errors++;
    }
    if (PROB(10)) { // SIGUSR1
        kill(pidToKill, SIGUSR1);
        fprintf(logFile, "SIGUSR1 ");
        errors++;
    }
    if (errors == 0) fprintf(logFile, "NO ERRORS ");
    fprintf(logFile, "\n");
    fflush(logFile);
    return 0;
}

void closeProcess() {
    fclose(logFile);
    close(masterChannel);
    exit(0);
}
