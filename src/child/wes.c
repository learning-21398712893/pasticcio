#include "../../lib/mainlib.h"
#include "../../lib/childlib.h"

int masterChannel;
char buffer[BUFFERSIZE];

char timeBuffer[16];

FILE *logFile; // status.log
int mySocket, switchChannel, managerChannel; // wes & manager socket

FILE *inputLogs[3]; // speedPFC1.log , speedPFC2.log , speedPFC3.log ,

// ----------------------------------------------------------------------------------------- Headers

void getTimeFromString(char *inputBuffer);

int evalBrokenPfc(int c1, int c2, int c3);

// ----------------------------------------------------------------------------------------- Main


int main() {
    childMain(&masterChannel, buffer, "wes");
}

int setup() {
    memset(buffer, '\0', BUFFERSIZE);
    memset(timeBuffer, '\0', 9);
    logFile = fopen("../logs/status.log", "w+");
    createSocket(SOCKET_WES, 2, &mySocket);
    return logFile == NULL || mySocket <= 0;
}

int preparation() {
    inputLogs[0] = fopen("../logs/speedPFC1.log", "r");
    inputLogs[1] = fopen("../logs/speedPFC2.log", "r");
    inputLogs[2] = fopen("../logs/speedPFC3.log", "r");
    int result = inputLogs[0] == NULL || inputLogs[1] == NULL || inputLogs[2] == NULL;
    switchChannel = accept(mySocket, NULL, NULL);
    return result || switchChannel <= 0;
}

int connection() {
    managerChannel = accept(mySocket, NULL, NULL);
    return managerChannel <= 0;
}

int routine() {
    read(managerChannel, buffer, BUFFERSIZE); // buffer contains string from manager
    getTimeFromString(buffer); // timeBuffer contains hh:mm:ss
    double readings[3] = {-1, -2, -3};
    for (int i = 0; i < 3; ++i) {
        fseek(inputLogs[i], 0, SEEK_CUR);
        fgets(buffer, BUFFERSIZE, inputLogs[i]);
        if (strlen(buffer) > 0) readings[i] = parseDouble(buffer);
    }
    int checkValues[3] = {readings[0] == readings[1], readings[1] == readings[2], readings[2] == readings[0]};
    int count = checkValues[0] + checkValues[1] + checkValues[2];
    if (count == 3) {
        printf("%s OK\n", timeBuffer);
        fprintf(logFile, "%s OK\n", timeBuffer);
        write(switchChannel, "0", BUFFERSIZE);
    } else if (count == 1) {
        printf("%s ERROR\n", timeBuffer);
        fprintf(logFile, "%s ERROR\n", timeBuffer);
        //
        int brokenPFC = evalBrokenPfc(checkValues[0], checkValues[1], checkValues[2]);
        char temp[32];
        sprintf(temp, "%d %s", brokenPFC, timeBuffer);
        write(switchChannel, temp, BUFFERSIZE);
    } else {
        printf("%s EMERGENCY\n", timeBuffer);
        fprintf(logFile, "%s EMERGENCY\n", timeBuffer);
        //
        char temp[32];
        sprintf(temp, "E %s", timeBuffer);
        write(switchChannel, temp, BUFFERSIZE);
    }
    fflush(logFile);
    return 0;
}


void closeProcess() {
    fclose(logFile);
    fclose(inputLogs[0]);
    fclose(inputLogs[1]);
    fclose(inputLogs[2]);
    close(switchChannel);
    close(mySocket);
    close(masterChannel);
    exit(0);
}

// ----------------------------------------------------------------------------------------- Complementary

void getTimeFromString(char *inputBuffer) {
    char tempBuffer[BUFFERSIZE];
    strcpy(tempBuffer, inputBuffer);
    char *tmp = strtok(tempBuffer, ",");
    for (int i = 0; i < 5; ++i)
        tmp = strtok(NULL, ",");
    int t = parseInt(tmp);
    int s = t % 100;
    int m = (t / 100) % 100;
    int h = t / 10000;
    sprintf(timeBuffer, "%s%d:%s%d:%s%d",
            h < 10 ? "0" : "", h,
            m < 10 ? "0" : "", m,
            s < 10 ? "0" : "", s
    );
}

int evalBrokenPfc(int c1, int c2, int c3) {
    if (c1 == 1) return 3;
    if (c2 == 1) return 1;
    if (c3 == 1) return 2;
    return 0;
}

void signalSwitch(char *string) {
    write(switchChannel, string, BUFFERSIZE);
}