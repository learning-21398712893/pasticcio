#include "../../lib/mainlib.h"
#include "../../lib/nephewlib.h"

int transducerChannel;
char buffer[BUFFERSIZE];

FILE *myLog;

FILE *myFile;

// ----------------------------------------------------------------------------------------- Main

int main(int argc, char **argv) {
    channelMain(&transducerChannel, buffer);
}

int setup() {
    myLog = fopen("../logs/speedPFC3.log", "w+");
    return myLog == NULL;
}

int preparation() {
    myFile = fopen(CHANNEL_FILE, "w+");
    return myFile == NULL;
}

int connection() {
    // do nothing
    return 0;
}

void routine() {
    bzero(buffer, BUFFERSIZE);
    fseek(myFile, 0, SEEK_CUR);
    fgets(buffer, BUFFERSIZE, myFile);
    fprintf(myLog, "%s\n", buffer);
    fflush(myLog);
}

void closeProcess() {
    fclose(myFile);
    remove(CHANNEL_FILE);
    fclose(myLog);
    close(transducerChannel);
    exit(0);
}
