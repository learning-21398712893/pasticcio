#include "../../lib/mainlib.h"
#include "../../lib/childlib.h"

int masterChannel;
char buffer[BUFFERSIZE];

FILE *logFile;

int wesChannel;

int PFC[3];

// ----------------------------------------------------------------------------------------- Headers

void checkPfcStatus(int pfcNumName);

int isProcessBlocked(int pid);

int isProcessDead(int pid);

// ----------------------------------------------------------------------------------------- Main


int main() {
    childMain(&masterChannel, buffer, "switch");
}

int setup() {
    logFile = fopen("../logs/switch.log", "w");
    return logFile == NULL;
}

int preparation() {
    connectSocket(SOCKET_WES, &wesChannel);
    return wesChannel <= 0;
}

int connection() {
    read(masterChannel, buffer, BUFFERSIZE);
    char *pfc = strtok(buffer, " ");
    int i = 0;
    while (pfc != NULL) {
        PFC[i++] = parseInt(pfc);
        pfc = strtok(NULL, " ");
    }
    return PFC[0] <= 0 && PFC[1] <= 0 && PFC[2] <= 0;
}

int routine() {
    read(wesChannel, buffer, BUFFERSIZE);
    char *time = strtok(buffer, " ");
    time = strtok(NULL, "\0");
    int terminateAll = 0;
    if (*buffer == 'E') {
        terminateAll = 1;
        fprintf(logFile, "%s EMERGENZA\n", time);
    } else {
        int pfc = parseInt(buffer);
        if (pfc == 0) return 0;
        else {
            fprintf(logFile, "%s ERROR - PFC%d", time, pfc);
            checkPfcStatus(pfc);
        }
    }
    fflush(logFile);
    return terminateAll;
}

void closeProcess() {
    close(masterChannel);
    exit(0);
}

// ----------------------------------------------------------------------------------------- Complementary

void checkPfcStatus(int pfcNumName) {
    int processPid = PFC[pfcNumName - 1];
    if (isProcessBlocked(processPid)) {
        kill(processPid, SIGCONT);
        fprintf(logFile, " [pid %d] - Processo interrotto e ripristinato\n", processPid);
    } else if (isProcessDead(processPid)) {
        fprintf(logFile, " [pid %d] - Processo terminato\n", processPid);
    } else {
        fprintf(logFile, " [pid %d] - Errore di comunicazione coordinate\n", processPid);
    }
    fflush(logFile);
}

int isProcessBlocked(int pid) {
    // TODO controllo stato del processo bloccato
    // return 1 se bloccato, return 0 altrimenti
    return 1;
}

int isProcessDead(int pid) {
    // TODO controllo stato del processo terminato
    // return 1 se terminato, return 0 altrimenti
    return 1;
}
