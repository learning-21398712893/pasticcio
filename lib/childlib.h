//
// Created by Alessandro Arfaioli on 04/02/2021.
//

#ifndef PROGETTO3_CHILDLIB_H
#define PROGETTO3_CHILDLIB_H

#include <signal.h>

// Da implementare

int setup();

int preparation();

int connection();

int routine();

void closeProcess();

// Implementate

void connectToMaster(int *channel, char *buffer);

void waitResponse(int *channel, char *buffer, int errors);

void startRoutine(int *channel, char *buffer, char *pname) {
    while (1) {
        //printf("%s e' in attesa di un segnale\n", pname);
        read(*channel, buffer, BUFFERSIZE);
        //printf("%s legge %s\n", pname, buffer);
        if (gotCONTSignal(buffer)) {
            int routineResult = routine();
            write(*channel, routineResult == 0 ? SIG_SUCC : SIG_FAIL, BUFFERSIZE);
        } else if (gotTERMSignal(buffer)) {
            break;
        } else {
            //printf("AMDOANDASDIASYDBYUAS\n");
        }
    }
}

void childMain(int *channel, char *buffer, char *name) {
    connectToMaster(channel, buffer);
    waitResponse(channel, buffer, setup());
    waitResponse(channel, buffer, preparation());
    waitResponse(channel, buffer, connection());
    startRoutine(channel, buffer, name);
    closeProcess();
}

void connectToMaster(int *channel, char *buffer) {
    connectSocket(SOCKET_MASTER, channel);
    sprintf(buffer, "%d", getpid());
    write(*channel, buffer, BUFFERSIZE);
    read(*channel, buffer, BUFFERSIZE);
    if (!gotCONTSignal(buffer)) closeProcess();
}

void waitResponse(int *channel, char *buffer, int errors) {
    sprintf(buffer, "%s", errors == 0 ? SIG_SUCC : SIG_FAIL);
    write(*channel, buffer, BUFFERSIZE);
    read(*channel, buffer, BUFFERSIZE);
    if (!gotCONTSignal(buffer)) closeProcess();
}

#endif //PROGETTO3_CHILDLIB_H
