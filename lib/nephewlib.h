//
// Created by Alessandro Arfaioli on 04/02/2021.
//

#ifndef PROGETTO3_NEPHEWLIB_H
#define PROGETTO3_NEPHEWLIB_H

#include <signal.h>

int setup();

int preparation();

int connection();

void routine();

void closeProcess();

// ----------------------------------------------------------------------------------------- Connect

void connectToManager(int *channel, char *buffer) {
    connectSocket(SOCKET_MANAGER, channel);
    sprintf(buffer, "%d", getpid());
    write(*channel, buffer, BUFFERSIZE);
    read(*channel, buffer, BUFFERSIZE);
    if (!gotCONTSignal(buffer)) closeProcess();
}

void connectToTransducer(int *channel, char *buffer) {
    connectSocket(SOCKET_TRANSDUCER, channel);
    sprintf(buffer, "%d", getpid());
    write(*channel, buffer, BUFFERSIZE);
    read(*channel, buffer, BUFFERSIZE);
    if (!gotCONTSignal(buffer)) closeProcess();
}

// ----------------------------------------------------------------------------------------- Wait Response

void waitResponse(int *channel, char *buffer, int errors) {
    sprintf(buffer, "%s", errors == 0 ? SIG_SUCC : SIG_FAIL);
    write(*channel, buffer, BUFFERSIZE);
    read(*channel, buffer, BUFFERSIZE);
    if (!gotCONTSignal(buffer)) closeProcess();
}

// ----------------------------------------------------------------------------------------- Routines

void startPfcRoutine(int *channel, char *buffer) {
    while (1) {
        read(*channel, buffer, BUFFERSIZE);
        if(gotTERMSignal(buffer)) break;
        else routine();
    }
}

void startChannelRoutine(int *channel, char *buffer) {
    while (1) {
        read(*channel, buffer, BUFFERSIZE);
        if (gotCONTSignal(buffer)) routine();
        else if (gotTERMSignal(buffer)) break;
    }
}

// ----------------------------------------------------------------------------------------- Main

void pfcMain(int *channel, char *buffer) {
    connectToManager(channel, buffer);
    waitResponse(channel, buffer, setup());
    waitResponse(channel, buffer, preparation());
    waitResponse(channel, buffer, connection());
    startPfcRoutine(channel, buffer);
    closeProcess();
}

void channelMain(int *channel, char *buffer) {
    connectToTransducer(channel, buffer);
    waitResponse(channel, buffer, setup());
    waitResponse(channel, buffer, preparation());
    waitResponse(channel, buffer, connection());
    startChannelRoutine(channel, buffer);
    closeProcess();
}

// ----------------------------------------------------------------------------------------- Pipes

void createNonBlockingPipe(char *pipeName, int *address) {
    mkfifo(pipeName, 0777);
    int channel = open(pipeName, O_RDONLY);
    fcntl(channel, F_SETFL, O_NONBLOCK);
    *address = channel;
}

void openNonBlockingPipe(char *pipeName, int *address) {
    int timeout = WAITTIME;
    int result = open(pipeName, O_WRONLY);
    while (result < 0 && timeout-- > 0) {
        sleep(1);
        result = open(pipeName, O_WRONLY);
    }
    *address = result;
}

// ----------------------------------------------------------------------------------------- Socket

void createNonBlockingSocket(char *s_name, int *sfd_address) {
    createSocket(s_name, 1, sfd_address);
    fcntl(*sfd_address, F_SETFL, O_NONBLOCK);
}

// ----------------------------------------------------------------------------------------- PFC

void extractValues(char *string, double *lon, double *lat) {
    double lat0, lon0;
    char tempBuffer[BUFFERSIZE];
    strcpy(tempBuffer, string);
    strtok(tempBuffer, ","); // rimuovi prefisso
    char *str = strtok(NULL, ","); // punta alla latitudine della coordinata
    lat0 = (double) parseDouble(str) / 100;
    str = strtok(NULL, ","); // punta al punto N/S della coordinata
    if (str[0] != 'N') lat0 = -lat0;
    str = strtok(NULL, ",");  // punta alla longitudine della coordinata
    lon0 = (double) parseDouble(str) / 100;
    str = strtok(NULL, ",");// punta al punto E/W della coordinata
    if (str[0] != 'E') lon0 = -lon0;
    *lat = lat0;
    *lon = lon0;
}

double degreesToRadians(double degrees) {
    return degrees * M_PI / 180;
}

double distanceBetween(double lon1, double lat1, double lon2, double lat2) {
    double earthRadiusMetres = 6371000;
    double dLat = degreesToRadians(lat2 - lat1);
    double dLon = degreesToRadians(lon2 - lon1);
    lat1 = degreesToRadians(lat1);
    lat2 = degreesToRadians(lat2);
    double a = sin(dLat / 2) * sin(dLat / 2) +
               sin(dLon / 2) * sin(dLon / 2) * cos(lat1) * cos(lat2);
    double c = 2 * atan2(sqrt(a), sqrt(1 - a));
    return earthRadiusMetres * c;
}

#endif //PROGETTO3_NEPHEWLIB_H
