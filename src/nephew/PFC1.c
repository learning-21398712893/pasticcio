#include "../../lib/mainlib.h"
#include "../../lib/nephewlib.h"

#include <signal.h>


int managerChannel;
char buffer[BUFFERSIZE];

int channel; // socket
int multiplier = 1;

int it = 0;
double lastLon, lastLat;

// ----------------------------------------------------------------------------------------- Headers

void sendResultToChannel(double result, int time);

void signal_handler(int signum) {
    if (signum == SIGUSR1) multiplier = 4;
}

// ----------------------------------------------------------------------------------------- Main

int main() {
    signal(SIGUSR1, signal_handler);
    pfcMain(&managerChannel, buffer);
}

int setup() {
    // do nothing
    return 0;
}

int preparation() {
    connectSocket(CHANNEL_SOCKET, &channel);
    return channel <= 0;
}

int connection() {
    // do nothing
    return 0;
}

void routine() {
    double lon, lat, distance;
    extractValues(buffer, &lon, &lat);
    distance = (it++ > 0) ? distanceBetween(lastLon, lastLat, lon, lat) * multiplier : 0;
    multiplier = 1;
    sprintf(buffer, "%f", distance);
    fflush(stdout);
    write(channel, buffer, BUFFERSIZE);
    lastLon = lon;
    lastLat = lat;
}

void closeProcess() {
    close(channel);
    close(managerChannel);
    exit(0);
}

// ----------------------------------------------------------------------------------------- Complementary

void sendResultToChannel(double result, int time) {
    sprintf(buffer, "%d: %f", time, result);
    write(channel, buffer, BUFFERSIZE);
    bzero(buffer, BUFFERSIZE);
}
