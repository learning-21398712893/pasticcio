cmake_minimum_required(VERSION 3.17)
project(learning-21398712893-progetto3-17bbb7ed60cd C)

set(CMAKE_C_STANDARD 99)

add_executable(master main.c)

add_executable(manager src/child/manager.c)
add_executable(transducer src/child/transducer.c)
add_executable(switch src/child/switch.c)
add_executable(wes src/child/wes.c)
add_executable(failgen src/child/failgen.c)

add_executable(pfc1 src/nephew/PFC1.c)
add_executable(pfc2 src/nephew/PFC2.c)
add_executable(pfc3 src/nephew/PFC3.c)
add_executable(channel1 src/nephew/CHANNEL1.c)
add_executable(channel2 src/nephew/CHANNEL2.c)
add_executable(channel3 src/nephew/CHANNEL3.c)